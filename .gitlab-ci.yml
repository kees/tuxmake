image:
  name: tuxmake/ci-python3.7

variables:
  LANG: C.UTF-8

.tuxmake:
  except:
    - schedules

.arm64:
  only:
    - branches@Linaro/tuxmake
  tags:
    - arm64

stages:
  - unit-tests
  - code-checks
  - integration-tests
  - build
  - publish

.unit-tests:
  extends: .tuxmake
  needs: []
  stage: unit-tests
  script:
    - make unit-tests

unit-tests-python3.8:
  extends: .unit-tests
  image: tuxmake/ci-python3.8

unit-tests-python3.7:
  extends: .unit-tests
  image: tuxmake/ci-python3.7

unit-tests-python3.6:
  extends: .unit-tests
  image: tuxmake/ci-python3.6

unit-tests-python3.8-arm64:
  extends: [.unit-tests, .arm64]
  image: tuxmake/ci-python3.8

unit-tests-python3.7-arm64:
  extends: [.unit-tests, .arm64]
  image: tuxmake/ci-python3.7

unit-tests-python3.6-arm64:
  extends: [.unit-tests, .arm64]
  image: tuxmake/ci-python3.6

.code-checks:
  extends: .tuxmake
  stage: code-checks
  needs: []

style:
  extends: .code-checks
  script:
    - make style

typecheck:
  extends: .code-checks
  script:
    - make typecheck

codespell:
  extends: .code-checks
  script:
    - make codespell

docker-build-tests:
  extends: .code-checks
  script:
    - make docker-build-tests

.integration:
  extends: .tuxmake
  stage: integration-tests
  needs: []
  before_script:
    - git status
    - rm -rf dist/
    - flit --debug build
    - python3 -m pip install dist/*.whl
  script:
    - make integration-tests TMV=1

integration-python3.8:
  extends: .integration
  image: tuxmake/ci-python3.8

integration-python3.7:
  extends: .integration
  image: tuxmake/ci-python3.7

integration-python3.6:
  extends: .integration
  image: tuxmake/ci-python3.7

integration-python3.8-arm64:
  extends: [.integration, .arm64]
  image: tuxmake/ci-python3.8

integration-python3.7-arm64:
  extends: [.integration, .arm64]
  image: tuxmake/ci-python3.7

integration-python3.6-arm64:
  extends: [.integration, .arm64]
  image: tuxmake/ci-python3.6

integration-docker:
  extends: integration-python3.8
  services:
    - name: docker:19.03-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  script:
    # Run integration tests as a non-root user, on purpose. HOME and TMPDIR
    # must be inside $(pwd) so that they are available to the docker server
    # (which is running in a separate container)
    - mkdir -p nobody/home nobody/tmp
    - chown -R nobody:nogroup nobody
    - runuser -u nobody make integration-tests-docker TMV=1 HOME=$(pwd)/nobody TMPDIR=$(pwd)/nobody/tmp

.docs:
  extends: .tuxmake
  stage: build
  needs: []
  image: tuxmake/ci-python3.8
  before_script:
    - apt-get update -q
    - python3 -m pip install mkautodoc  # FIXME not in Debian
  script:
    - make man
    - make doc

build-docs:
  extends: .docs
  except:
    - master

pages:
  extends: .docs
  artifacts:
    paths:
      - public
  only:
    - master

.docker:
  image: docker:19.03.11-dind
  services:
    - name: docker:19.03.11-dind
  only:
    - schedules
  variables:
    DOCKER_DRIVER: overlay2
  before_script:
    - apk add make python3
    - cd support/docker/
    - ./configure
    - 'docker login --username="${DOCKER_USERNAME}" --password-stdin < "${DOCKER_PASSWORD_FILE}"'

docker-images-amd64:
  stage: build
  extends: .docker
  variables:
    TAG: "-amd64"
  script:
    - "make $(for img in ${DOCKER_IMAGES}; do echo publish-${img}; done)"

docker-images-arm64:
  stage: build
  extends: .docker
  tags:
    - arm64-dind
  variables:
    TAG: "-arm64"
  script:
    - "make $(for img in ${DOCKER_IMAGES}; do echo publish-${img}; done)"

docker-images-multiarch:
  stage: publish
  needs:
    - docker-images-amd64
    - docker-images-arm64
  extends: .docker
  variables:
    DOCKER_CLI_EXPERIMENTAL: "enabled"
    ARCH_TAGS: "-amd64 -arm64"
  script:
    - "make $(for img in ${DOCKER_IMAGES}; do echo publish-multiarch-${img}; done)"
