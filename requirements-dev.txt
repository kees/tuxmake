pytest
pytest-cov
pytest-mock
flit
black
flake8
mypy
codespell
docutils
mkdocs
# you also need these non-Python packages: make shunit2 git ccache gcc
